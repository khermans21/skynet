#!/bin/bash

rm -rf ./build/*
cd build
cmake -DSKYNET_MATRIX_SOLVER=mkl -DCMAKE_INSTALL_PREFIX=/home/khermans/soft/skynet/install ..



# then in the build directory run 
make -j8 install 
