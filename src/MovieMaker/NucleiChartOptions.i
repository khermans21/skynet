%{
#include "MovieMaker/NucleiChartOptions.hpp"
%}

%ignore str_const;
%ignore *::operator[];

%include "MovieMaker/NucleiChartOptions.hpp"