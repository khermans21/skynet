/// \file ReactionNetwork.cpp
/// \author jlippuner
/// \since Oct 31, 2014
///
/// \brief
///
///

// these is not the cleanest way to split up a class into multiple .cpp files,
// but it gets the job done and avoids linking issues
#include "Network/ReactionNetwork_ConstructorsEvolveOverloads.cpp"
#include "Network/ReactionNetwork_EvolutionImplementation.cpp"
#include "Network/ReactionNetwork_NSEEvolution.cpp"


