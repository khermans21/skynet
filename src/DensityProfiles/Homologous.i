%{
#include "Utilities/FunctionVsTime.hpp"
#include "DensityProfiles/Homologous.hpp"
%}

%ignore MakeUniquePtr;

%include "Utilities/FunctionVsTime.hpp"
%include "DensityProfiles/Homologous.hpp"
