%{
#include "Utilities/FunctionVsTime.hpp"
#include "DensityProfiles/PowerLawContinuation.hpp"
%}

%ignore MakeUniquePtr;

%include "Utilities/FunctionVsTime.hpp"
%include "DensityProfiles/PowerLawContinuation.hpp"
