%{
#include "EquationsOfState/EOS.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
%}

%include "EquationsOfState/EOS.hpp"
%include "EquationsOfState/HelmholtzEOS.hpp"
