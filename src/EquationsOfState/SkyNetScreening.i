%{
#include "EquationsOfState/Screening.hpp"
#include "EquationsOfState/SkyNetScreening.hpp"
%}

%include "EquationsOfState/Screening.hpp"
%include "EquationsOfState/SkyNetScreening.hpp"
