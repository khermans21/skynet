%{
#include "Utilities/FunctionVsTime.hpp"
#include "EquationsOfState/NeutrinoDistribution.hpp"
#include "EquationsOfState/NeutrinoHistory.hpp"
%}

%include "Utilities/FunctionVsTime.hpp"
%include "EquationsOfState/NeutrinoDistribution.hpp"
%include "EquationsOfState/NeutrinoHistory.hpp"
