%{
#include "Reactions/ReactionLibraryBase.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
%}

%include "Reactions/ReactionLibraryBase.hpp"
%include "Reactions/REACLIBReactionLibrary.hpp"
