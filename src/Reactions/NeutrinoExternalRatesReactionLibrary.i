%{
#include "Reactions/ReactionLibraryBase.hpp"
#include "Reactions/NeutrinoExternalRatesReactionLibrary.hpp"
%}

%include "Reactions/ReactionLibraryBase.hpp"
%include "Reactions/NeutrinoExternalRatesReactionLibrary.hpp"
