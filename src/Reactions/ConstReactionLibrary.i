%{
#include "Reactions/ReactionLibraryBase.hpp"
#include "Reactions/ConstReactionLibrary.hpp"
%}

%include "Reactions/ReactionLibraryBase.hpp"
%include "Reactions/ConstReactionLibrary.hpp"
